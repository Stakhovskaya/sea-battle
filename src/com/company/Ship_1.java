package com.company;

public class Ship_1 {
    private double x_coordinate;
    private double y_coordinate;
    public Ship_1(){
        x_coordinate = Math.round(Math.random()*10) -1;
        y_coordinate = Math.round(Math.random()*10) -1;
    }

    public double getX_coordinate() {
        return x_coordinate;
    }

    public double getY_coordinate() {
        return y_coordinate;
    }
}
