package com.company;

import java.util.Scanner;

public class Main {
    //    public static double [][] Ships_1_coordinate = new double[4][2];
    public static void main(String[] args) {
        // write your code here
        Scanner enter = new Scanner(System.in);
        char [][] pole = new char[10][10];
        Ship_1 ship_11 = new Ship_1();
        Ship_1 ship_12 = new Ship_1();
        Ship_1 ship_13 = new Ship_1();
        Ship_1 ship_14 = new Ship_1();
        Ship_1 [] ship_1s = {
                ship_11,ship_12,ship_13,ship_14
        };
        Ship_1 ship_21 = new Ship_1();
        Ship_1 ship_22 = new Ship_1();
        Ship_1 ship_23 = new Ship_1();
        Ship_1 [] ship_2s = {
                ship_21, ship_22, ship_23
        };
        Ship_1 ship_31 = new Ship_1();
        Ship_1 ship_32 = new Ship_1();
        Ship_1 [] ship_3s = {
                ship_31, ship_32
        };
        Ship_1 ship_41 = new Ship_1();

        int choice = 1;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                pole[i][j] = '-';
            }
        }
        System.out.println("ships_1s");
        for (int i = 0; i < ship_1s.length; i++) {
            System.out.print("X"+ (i+1) +" = "+ ship_1s[i].getX_coordinate());
            System.out.println(" Y"+(i+1)+"= "+ship_1s[i].getY_coordinate());
        }

        System.out.println("ships_2s");
        for (int i = 0; i < ship_2s.length; i++) {
            System.out.print("X"+ (i+1) +" = "+ ship_2s[i].getX_coordinate());
            System.out.println(" Y"+(i+1)+"= "+ship_2s[i].getY_coordinate());
        }

        System.out.println("ships_3s");
        for (int i = 0; i < ship_3s.length; i++) {
            System.out.print("X"+ (i+1) +" = "+ ship_3s[i].getX_coordinate());
            System.out.println(" Y"+(i+1)+"= "+ship_3s[i].getY_coordinate());
        }

        System.out.println("ships_41");
        System.out.print("X= "+ ship_41.getX_coordinate());
        System.out.println(" Y= "+ship_41.getY_coordinate());

        while (choice == 1){

            showPole(pole);
            System.out.println("Enter X");
            int x = enter.nextInt();
            System.out.println("Enter Y");
            int y = enter.nextInt();
            pole = shot(ship_1s, ship_2s,ship_3s,ship_41,pole, x, y);
            showPole(pole);
            System.out.println("Get Shot?\n\t1-yes\n\t2-no");
            choice = enter.nextInt();
        }

    }
    public static char[][] shot( Ship_1 [] ship_1s,  Ship_1 [] ship_2s,  Ship_1 [] ship_3s,  Ship_1 ship_41, char[][] pole,int x, int y){
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                for (int k = 0; k < ship_1s.length; k++) {
                    if (ship_1s[k].getX_coordinate()== x && ship_1s[k].getY_coordinate() == y) {
                        System.out.println(ship_1s.length);
                        System.out.println(k);
                        if (pole[y][x] == '-'){
                            pole[y][x] = 'X';
                        }
                        else if (pole[y][x] == 'X'){
                            System.out.println("Сюда Вы уже стреляли - 1");
                        }
                    }
                }
                for (int k = 0; k < ship_2s.length; k++) {
                    if (ship_2s[k].getX_coordinate() == x && ship_2s[k].getY_coordinate() == y) {
                        if (pole[y][x] == '-') {
                            pole[y][x] = 'X';
                        } else if (pole[y][x] == 'X') {
                            System.out.println("Сюда Вы уже стреляли - 2");
                        }
                    }
                }
                for (int k = 0; k < ship_3s.length; k++) {
                    if (ship_3s[k].getX_coordinate() == x && ship_3s[k].getY_coordinate() == y) {
                        if (pole[y][x] == '-') {
                            pole[y][x] = 'X';
                        } else if (pole[y][x] == 'X') {
                            System.out.println("Сюда Вы уже стреляли - 3");
                        }
                    }
                }

                if(ship_41.getX_coordinate()== x && ship_41.getY_coordinate() == y){
                    if (pole[y][x] == '-') {
                        pole[y][x] = 'X';
                    } else if (pole[y][x] == 'X') {
                        System.out.println("Сюда Вы уже стреляли - 4");
                    }
                }
                if (i == y && j == x) {
                    if (pole[y][x] == '-'){
                        pole[y][x] = '*';
                    }
                    else if (pole[y][x] == '*'){
                        System.out.println("Сюда Вы уже стреляли");
                    }
                }
            }
        }
        return pole;
    }
    public static void showPole( char[][] pole){
        for (int k = 0; k < 10; k++) {
            if (k == 0) {
                System.out.print("x/y");
            }
            System.out.print("| "+ (k+1) +" | ");
        }
        System.out.println("");
        for (int i = 0; i < 10; i++) {
            System.out.print((i+1) + ") ");
            for (int j = 0; j < 10; j++) {
                System.out.print("| "+pole[i][j]+" | ");
            }
            System.out.println("");
        }
    }
//    public static

}